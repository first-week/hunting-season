package org.example.huntingseason;

public class Frog extends Animal {

    public Frog() {
        super("Frog");
    }

    @Override
    public void sayBeforeDeath() {
        System.out.println("Ква ква!");
    }
}
