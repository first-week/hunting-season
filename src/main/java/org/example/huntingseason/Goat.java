package org.example.huntingseason;

public class Goat extends Animal {

    public Goat() {
        super("Goatee");
    }

    @Override
    public void sayBeforeDeath() {
        System.out.println("Бееееее!");
    }
}
