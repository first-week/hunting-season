package org.example.huntingseason;

public class Gosse extends Animal {

    public Gosse() {
        super("Gosse");
    }

    @Override
    public void sayBeforeDeath() {
        System.out.println("Кря кря!");
    }
}
