package org.example.huntingseason;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Hunting {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        Hunter hunter = context.getBean(Hunter.class);

        Animal gosse = context.getBean("gosse", Animal.class);
        Animal goat = context.getBean("goat", Animal.class);
        Animal frog = context.getBean("frog", Animal.class);

        hunter.setCurrentAnimal(gosse);
        hunter.killAnimal();

        hunter.setCurrentAnimal(goat);
        hunter.killAnimal();

        hunter.setCurrentAnimal(frog);
        hunter.killAnimal();


    }
}
