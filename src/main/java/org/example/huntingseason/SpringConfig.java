package org.example.huntingseason;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfig {

    @Bean
    public Animal gosse() {
        return new Gosse();
    }

    @Bean
    public Animal goat() {
        return new Goat();
    }

    @Bean
    public Animal frog() {
        return new Frog();
    }

    @Bean
    public Hunter hunter() {
        return new Hunter();
    }
}
